#include "senderthread.h"

#include <QEventLoop>
#include <QNetworkReply>



void SenderThread::run()
{
    while (true)
    {
        while (!m_queue.isEmpty())
        {
            QUrl url(m_queue.front());

            QNetworkReply *reply = m_nam.get(QNetworkRequest(url));

            QEventLoop eventLoop;
            QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
            eventLoop.exec();

            if (reply->error() == QNetworkReply::NoError)
            {
                qDebug() << m_queue.front() << "is sent";
                m_queue.pull();
            }

            if (m_queue.isEmpty())
            {
                emit sendReady();
            }
        }

        sleep(1);
    }
}

void SenderThread::send(const QString &urlStr)
{
    m_queue.push(urlStr);
}
