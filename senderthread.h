#ifndef SENDERTHREAD_H
#define SENDERTHREAD_H

#include <QNetworkAccessManager>
#include <QObject>
#include <QThread>
#include "qasyncqueue.h"

class SenderThread : public QThread
{
    Q_OBJECT

public:
    void run();

public slots:
    void send(QString const& urlStr);

signals:
    void sendReady();

private:
    QAsyncQueue<QString> m_queue;
    QNetworkAccessManager m_nam;
};

#endif // SENDERTHREAD_H
