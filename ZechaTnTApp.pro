#-------------------------------------------------
#
# Project created by QtCreator 2016-03-09T23:20:37
#
#-------------------------------------------------

QT       += core gui positioning network
QT += androidextras

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ZechaTnTApp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    senderthread.cpp

HEADERS  += mainwindow.h \
    qasyncqueue.h \
    senderthread.h

FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    res.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

