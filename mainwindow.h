#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "senderthread.h"

#include <QMainWindow>
#include <QGeoCoordinate>
#include <QNetworkAccessManager>
#include <QStateMachine>
#include <QGeoPositionInfoSource>
#include <QVector>

namespace Ui {
class MainWindow;
}

class QGeoPositionInfo;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    typedef QPair<int, QString> IntStringPair_t;

    enum State
    {
        AWAY,
        AT_WORK,
        TASK,
        BREAK,
        NAME_SEL,
        TASK_SEL
    };


    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void keyReleaseEvent(QKeyEvent *e);

    void initStateMachine();

    void applyStyle();

private slots:

    void fillNamesBox();
    void fillTaskBox();
    void positionUpdated(const QGeoPositionInfo &info);
    void updateTimes();
    void resetTimes();
    void nameSelected(int index);
    void taskSelected(int index);
    void hideAll();
    void sendTime();
    void initWorkStart();
    void initTaskStart();
    void initBreakStart();
    void sendReady();
    void resetTaskId();
    void navigateCustomer();
    void navigateHome();
private:

    QGeoPositionInfoSource *m_geoSource;
    State getState();

    Ui::MainWindow *ui;
    QGeoCoordinate m_oldCoordinate;
    QNetworkAccessManager m_nam;
    QStateMachine m_machine;
    SenderThread *m_sendThread;

    QDateTime m_workStart;
    QDateTime m_taskStart;
    QDateTime m_breakStart;

    QTime m_workTimer;
    QTime m_taskTimer;
    QTime m_breakTimer;

    int m_nameId;
    int m_taskId;

    QVector<IntStringPair_t> readValues(QString urlStr);
    void sendUpdate(QString urlStr);
    QDateTime determineFromTime();
    bool wasBreak();
};

#endif // MAINWINDOW_H
