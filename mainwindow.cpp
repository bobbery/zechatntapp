#include "mainwindow.h"
#include "ui_mainwindow.h"


#include <QNetworkReply>
#include <QUrl>
#include <QDateTime>
#include <QStateMachine>
#include <QKeyEvent>
#include <QtCore>
#include <QMessageBox>
#include <QtAndroid>
#include <QAndroidJniObject>
#include <QDesktopServices>
#include <QTextDocument>

static const QString BASE_URL = "http://zecha1.roehrach.de";


void MainWindow::initStateMachine()
{
    QState *sNameSelect = new QState();
    QState *sAway = new QState();
    QState *sAtWork = new QState();
    QState *sTaskSel = new QState();
    QState *sTask = new QState();
    QState *sBreak = new QState();
    QState *sBreakTask = new QState();

    sNameSelect->addTransition(ui->btnNameOk, SIGNAL(clicked()), sAway);
    sNameSelect->assignProperty(ui->lblState, "text", "Abwesend");
    connect(sNameSelect, SIGNAL(entered()), this, SLOT(hideAll()));
    connect(sNameSelect, SIGNAL(entered()), ui->btnNameOk, SLOT(show()));
    connect(sNameSelect, SIGNAL(entered()), ui->cmbName, SLOT(show()));

    sAway->addTransition(ui->btnWorkStart, SIGNAL(clicked()), sAtWork);
    sAway->assignProperty(ui->lblState, "text", "Abwesend");
    connect(sAway, SIGNAL(entered()), this, SLOT(hideAll()));
    connect(sAway, SIGNAL(entered()), ui->btnWorkStart, SLOT(show()));
    connect(sAway, SIGNAL(entered()), m_geoSource, SLOT(stopUpdates()));
    connect(sAway, SIGNAL(exited()), this, SLOT(resetTimes()));

    sAtWork->addTransition(ui->btnWorkEnd, SIGNAL(clicked()), sAway);
    sAtWork->addTransition(ui->btnTaskStart, SIGNAL(clicked()), sTaskSel);
    sAtWork->addTransition(ui->btnBreakStart, SIGNAL(clicked()), sBreak);
    sAtWork->assignProperty(ui->lblState, "text", "Auf Arbeit");
    connect(sAtWork, SIGNAL(entered()), this, SLOT(resetTaskId()));
    connect(sAtWork, SIGNAL(entered()), this, SLOT(initWorkStart()));
    connect(sAtWork, SIGNAL(entered()), this, SLOT(hideAll()));
    connect(sAtWork, SIGNAL(entered()), ui->btnWorkEnd, SLOT(show()));
    connect(sAtWork, SIGNAL(entered()), ui->btnTaskStart, SLOT(show()));
    connect(sAtWork, SIGNAL(entered()), ui->btnBreakStart, SLOT(show()));
    connect(sAtWork, SIGNAL(entered()), m_geoSource, SLOT(startUpdates()));
    connect(sAtWork, SIGNAL(exited()), this, SLOT(sendTime()));

    sTaskSel->addTransition(ui->btnTaskOk, SIGNAL(clicked()), sTask);
    connect(sTaskSel, SIGNAL(entered()), this, SLOT(hideAll()));
    connect(sTaskSel, SIGNAL(entered()), this, SLOT(fillTaskBox()));
    connect(sTaskSel, SIGNAL(entered()), ui->btnTaskOk, SLOT(show()));
    connect(sTaskSel, SIGNAL(entered()), ui->cmbTask, SLOT(show()));

    sTask->addTransition(ui->btnTaskEnd, SIGNAL(clicked()), sAtWork);
    sTask->addTransition(ui->btnBreakStart, SIGNAL(clicked()), sBreakTask);
    sTask->assignProperty(ui->lblState, "text", "Auftrag");
    connect(sTask, SIGNAL(entered()), this, SLOT(initTaskStart()));
    connect(sTask, SIGNAL(entered()), this, SLOT(hideAll()));
    connect(sTask, SIGNAL(entered()), ui->btnTaskEnd, SLOT(show()));
    connect(sTask, SIGNAL(entered()), ui->btnBreakStart, SLOT(show()));
    connect(sTask, SIGNAL(entered()), ui->btNavCustomer, SLOT(show()));
    connect(sTask, SIGNAL(entered()), ui->btnNavHome, SLOT(show()));

    connect(sTask, SIGNAL(entered()), m_geoSource, SLOT(startUpdates()));
    connect(sTask, SIGNAL(exited()), this, SLOT(sendTime()));

    sBreak->addTransition(ui->btnBreakEnd, SIGNAL(clicked()), sAtWork);
    sBreak->assignProperty(ui->lblState, "text", "Pause");
    connect(sBreak, SIGNAL(entered()), this, SLOT(initBreakStart()));
    connect(sBreak, SIGNAL(entered()), this, SLOT(hideAll()));
    connect(sBreak, SIGNAL(entered()), ui->btnBreakEnd, SLOT(show()));
    connect(sBreak, SIGNAL(entered()), m_geoSource, SLOT(stopUpdates()));
    connect(sBreak, SIGNAL(exited()), this, SLOT(sendTime()));

    sBreakTask->addTransition(ui->btnBreakEnd, SIGNAL(clicked()), sTask);
    sBreakTask->assignProperty(ui->lblState, "text", "Pause");
    connect(sBreakTask, SIGNAL(entered()), this, SLOT(initBreakStart()));
    connect(sBreakTask, SIGNAL(entered()), this, SLOT(hideAll()));
    connect(sBreakTask, SIGNAL(entered()), ui->btnBreakEnd, SLOT(show()));
    connect(sBreakTask, SIGNAL(entered()), m_geoSource, SLOT(stopUpdates()));
    connect(sBreakTask, SIGNAL(exited()), this, SLOT(sendTime()));

    m_machine.addState(sNameSelect);
    m_machine.addState(sAway);
    m_machine.addState(sAtWork);
    m_machine.addState(sTaskSel);
    m_machine.addState(sTask);
    m_machine.addState(sBreak);
    m_machine.addState(sBreakTask);
    m_machine.setInitialState(sNameSelect);

    sNameSelect->assignProperty(&m_machine, "state", NAME_SEL);
    sAway->assignProperty(&m_machine, "state", AWAY);
    sAtWork->assignProperty(&m_machine, "state", AT_WORK);
    sTaskSel->assignProperty(&m_machine, "state", TASK_SEL);
    sTask->assignProperty(&m_machine, "state", TASK);
    sBreak->assignProperty(&m_machine, "state", BREAK);
    sBreakTask->assignProperty(&m_machine, "state", BREAK);
}


QVector<MainWindow::IntStringPair_t> MainWindow::readValues(QString urlStr)
{
    QVector<IntStringPair_t> result;
    QUrl url(urlStr);

    QNetworkReply *reply = m_nam.get(QNetworkRequest(url));

    QEventLoop eventLoop;
    QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();

    QString namesStr = reply->readAll();

    namesStr.chop(2);
    QStringList names = namesStr.split('\n');

    foreach (QString idAndName, names)
    {
        QStringList idName = idAndName.split("$$");

        if (idName.size() == 2)
        {
            QTextDocument doc;
            doc.setHtml( idName.at(1) );
            result.push_back(qMakePair(idName.at(0).toInt(), doc.toPlainText()));
        }
    }

    return result;
}


void MainWindow::fillNamesBox()
{
    QString urlStr = BASE_URL + "/names.php";
    QVector<IntStringPair_t> result = readValues(urlStr);

    for (IntStringPair_t pair : result)
    {
        ui->cmbName->addItem(pair.second, pair.first);
    }
}

void MainWindow::fillTaskBox()
{
    ui->cmbTask->clear();
    ui->cmbTask->addItem("Auftrag bitte");

    QString urlStr = BASE_URL + "/tasks.php";
    QVector<IntStringPair_t> result = readValues(urlStr);

    for (IntStringPair_t pair : result)
    {
        ui->cmbTask->addItem(pair.second, pair.first);
    }
}

void MainWindow::positionUpdated(const QGeoPositionInfo &info)
{
    const qreal minDist = 10.0;

    QDateTime now = QDateTime::currentDateTime();
    QString timeStr = now.toString("yyyyMMddhhmmss");

    if (info.coordinate().distanceTo(m_oldCoordinate) > minDist || (!m_oldCoordinate.isValid() && info.coordinate().isValid()))
    {
        QString urlStr = BASE_URL + "/track.php" +
                "?name=" + QString::number(m_nameId) +
                "&lon=" + QString::number(info.coordinate().longitude()) +
                "&lat=" + QString::number(info.coordinate().latitude()) +
                "&time=" + timeStr +
                "&task=" + QString::number(m_taskId);

        sendUpdate(urlStr);

        m_oldCoordinate = info.coordinate();
    }
}

void MainWindow::sendTime()
{
    const int WORK_TYPE = 1;
    const int BREAK_TYPE = 2;

    QDateTime from = determineFromTime();
    QString fromStr = from.toString("yyyyMMddhhmmss");

    QDateTime now = QDateTime::currentDateTime();
    QString nowStr = now.toString("yyyyMMddhhmmss");
    QString type = wasBreak() ? QString::number(BREAK_TYPE) : QString::number(WORK_TYPE);

    QString urlStr = QString(BASE_URL) + "/time.php" +
            "?name=" + QString::number(m_nameId) +
            "&type=" + type +
            "&task=" + QString::number(m_taskId) +
            "&from=" + fromStr +
            "&to=" + nowStr;

    sendUpdate(urlStr);
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_nameId(0),
    m_taskId(0)
{
    ui->setupUi(this);
    applyStyle();


    m_sendThread = new SenderThread();

    connect(m_sendThread, SIGNAL(sendReady()),this, SLOT(sendReady()));


    connect(ui->btNavCustomer, SIGNAL(clicked()),this, SLOT(navigateCustomer()));
    connect(ui->btnNavHome, SIGNAL(clicked()),this, SLOT(navigateHome()));


    m_sendThread->start();

    m_geoSource = QGeoPositionInfoSource::createDefaultSource(this);

    if (m_geoSource)
    {
        connect(m_geoSource, SIGNAL(positionUpdated(QGeoPositionInfo)),this, SLOT(positionUpdated(QGeoPositionInfo)));
    }

    fillNamesBox();

    connect(ui->cmbName, SIGNAL(activated(int)), this, SLOT(nameSelected(int)));
    connect(ui->cmbTask, SIGNAL(activated(int)), this, SLOT(taskSelected(int)));

    resetTimes();

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTimes()));
    timer->start(1000);

    initStateMachine();

    m_machine.start();
}


void MainWindow::updateTimes()
{
    if (getState() == AT_WORK || getState() == TASK)
    {
        m_workTimer = m_workTimer.addSecs(1);
    }

    if (getState() == BREAK)
    {
        m_breakTimer = m_breakTimer.addSecs(1);
    }

    if (getState() == TASK)
    {
        m_taskTimer = m_taskTimer.addSecs(1);
    }

    ui->lblWorkTime->setText(m_workTimer.toString());
    ui->lblBreakTime->setText(m_breakTimer.toString());
    ui->lblTaskTime->setText(m_taskTimer.toString());
}

void MainWindow::resetTimes()
{
    m_workTimer = QTime(0,0,0);
    m_taskTimer = QTime(0,0,0);
    m_breakTimer = QTime(0,0,0);
}

void MainWindow::nameSelected(int index)
{
    if (index != 0)
    {
        ui->btnNameOk->setEnabled(true);
        m_nameId = ui->cmbName->itemData(index).toInt();
    }
    else
    {
        ui->btnNameOk->setEnabled(false);
    }
}

void MainWindow::taskSelected(int index)
{
    if (index != 0)
    {
        ui->btnTaskOk->setEnabled(true);
        m_taskId = ui->cmbTask->itemData(index).toInt();
    }
    else
    {
        ui->btnTaskOk->setEnabled(false);
    }
}

void MainWindow::sendUpdate(QString urlStr)
{
    ui->lblZecha->setPixmap(QPixmap(":/zecha1.png"));
    m_sendThread->send(urlStr);
}

void MainWindow::sendReady()
{
    ui->lblZecha->setPixmap(QPixmap(":/zecha.png"));
}

void MainWindow::resetTaskId()
{
    m_taskId = 0;
}

void MainWindow::navigateCustomer()
{

    QString urlStr = BASE_URL + "/address.php?id=" + QString::number(m_taskId);
    QVector<IntStringPair_t> result = readValues(urlStr);

    QString link = "geo:0,0?q=Wiesbadener+Straße+51+90427+Nürnberg";

    if (!result.empty())
    {
        QString address = result.at(0).second;
        address.replace(" ", "+");
        link = "geo:0,0?q=" + address;
    }

    qDebug() << link;

    QDesktopServices::openUrl(link);
}

void MainWindow::navigateHome()
{
    QString link = "geo:0,0?q=Wiesbadener+Straße+51+90427+Nürnberg";
    qDebug() << link;
    QDesktopServices::openUrl(link);
}


QDateTime MainWindow::determineFromTime()
{
    if (getState() == BREAK)
    {
        return m_breakStart;
    }
    else if (getState() == TASK)
    {
        return m_taskStart;
    }
    else
    {
        return m_workStart;
    }
}

bool MainWindow::wasBreak()
{
    if (getState() == BREAK)
    {
        return true;
    }
    else
    {
        return false;
    }
}


void MainWindow::initWorkStart()
{
    m_workStart = QDateTime::currentDateTime();
}

void MainWindow::initTaskStart()
{
    m_taskStart = QDateTime::currentDateTime();
}

void MainWindow::initBreakStart()
{
    m_breakStart = QDateTime::currentDateTime();
}

void MainWindow::hideAll()
{
    ui->btnWorkStart->hide();
    ui->btnWorkEnd->hide();
    ui->btnTaskStart->hide();
    ui->btnTaskEnd->hide();
    ui->btnBreakStart->hide();
    ui->btnBreakEnd->hide();
    ui->btnNameOk->hide();
    ui->btnTaskOk->hide();
    ui->cmbName->hide();
    ui->cmbTask->hide();
    ui->btNavCustomer->hide();
    ui->btnNavHome->hide();
}

MainWindow::State MainWindow::getState()
{
    return static_cast<State>(m_machine.property("state").toInt());
}

void MainWindow::applyStyle()
{
    QFile styleFile( ":/zecha.qss" );
    styleFile.open( QFile::ReadOnly );
    QString style( styleFile.readAll() );
    qApp->setStyleSheet(style);
    styleFile.close();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyReleaseEvent(QKeyEvent *e)
{
    if (e->key() == Qt::BackButton)
    {
        e->accept();
    }
}
